import java.util.Arrays;
import java.util.Scanner;

/**
 * Требуется определить максимальную прибыль для компании по известным
 * времени и стоимости заказов
 *
 */
public class WarmUp {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        /**
         * Получение исходных данных
         */
        System.out.println("Введите доступное для компании время: ");
        int time = scanner.nextInt();

        System.out.println("Введите кол-во доступных заказов: ");
        int orders_count = scanner.nextInt();

        int[] orders = new int[orders_count];
        for(int i = 0; i < orders_count; i++) {
            System.out.printf("Введите прибыль от %d заказа: ", i + 1);
            orders[i] = scanner.nextInt();
        }

        /**
         * Сортировка массива по возрастанию
         */
        Arrays.sort(orders);

        /**
         * На самом деле кол-во заказов, которые компания может выполнить зависит от двух величин
         * и равно наименьшей из них. Т. о. елси компании доступно время time и count_orders заказов
         * то компания сможет выполнить Math.min(orders_count, time)
         */
        orders_count = Math.min(orders_count, time);
        int profit = 0;

        /**
         * Подсчёт прибыли путём сложения прибыли от самых выгодных заказов
         */
        for(int i = 1; i <= orders_count; i++) {
            profit += orders[orders.length - i];
        }


        System.out.printf("За время %d, компания сможет выполнить %d заказов. Максимальная прибыль = %d", time, orders_count, profit);
    }
}

